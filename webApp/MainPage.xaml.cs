﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebAppConsuming.Model
using Xamarin.Forms;

namespace WebAppConsuming
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            GetList();

            //HttpClient client = new HttpClient();
            //var reponse = client.GetStringAsync("http://dummy.restapiexample.com/api/v1/employees");

            //var list = JsonConvert.DeserializeObject<Example>(reponse);


        }



        IList<EmployeeData> employeeDatas;
        List<Example> examples;

       async void GetList()
        {
            HttpClient client = new HttpClient();
           
            var reponse = await client.GetStringAsync("http://dummy.restapiexample.com/api/v1/employees");
            
            var data =  JsonConvert.DeserializeObject<Example>(reponse);

            employeeDatas = data.data;

            listView.ItemsSource = employeeDatas;
           // return employeeDatas;
        }
    }

   
   


    public class EmployeeData
    {
        public string id { get; set; }
        public string employee_name { get; set; }
        public string employee_salary { get; set; }
        public string employee_age { get; set; }
        public string profile_image { get; set; }
    }

    public class Example
    {
        public string status { get; set; }
        public IList<EmployeeData> data { get; set; }
    }

}
