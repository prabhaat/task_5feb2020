﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


class person
{
    public string Name
    {
        get;
        set;

    }
    public int Age { get; set; }
}

namespace _13feb2020
{
    class Program
    {
        static void Mini()
        {


            string str = "second time hello world";
            ///creating a file and appending into it using file system
            ///implementing the exception handling here too
            try
            {

               FileStream file = new FileStream("C:\\Users\\prabhatk\\Documents\\he.txt", FileMode.Append, FileAccess.Write);
               StreamWriter streamwriter = new StreamWriter(file);
               streamwriter.WriteLine(str);
               streamwriter.Close();
                file.Close();
                Console.WriteLine("file appended");
            }
            catch(FileNotFoundException e )
            {
                Console.WriteLine("file not found");

            }
            finally
            {
                Console.WriteLine("file system work is done");
            }



            ///writing into a file 
            ///implementing exception handling too 
            
            try
            {
                StreamReader streamreader = new StreamReader("C:\\Users\\prabhatk\\Documents\\fileRead.txt");

                //streamreader.BaseStream.Seek(0, SeekOrigin.Begin);
                //while ((streamreader.ReadToEnd() != null)
                //{

                    str = streamreader.ReadToEnd();

                    Console.WriteLine($"{str}");
                //}
            }
            catch(FileNotFoundException e)
            {
                Console.WriteLine("file not found");
                throw e;
            }

            Console.ReadKey();

        }
    }
}
