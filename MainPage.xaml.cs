﻿using ProfilePage.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProfilePage
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        DataViewModel dataViewModel = new DataViewModel();
        public MainPage()
        {
            InitializeComponent();
            BindingContext = dataViewModel;
            dataViewModel.Name = "Prabhat";
            dataViewModel.Location = "Noida";
            dataViewModel.Phone = "7303068809";
            dataViewModel.Type = "Student";
            dataViewModel.Email = "prabhat.kumar@Successive.tech";
            dataViewModel.CreatedDate = "21 April 2020";
            dataViewModel.AccNum = "99108399951";
        }
    }
}
