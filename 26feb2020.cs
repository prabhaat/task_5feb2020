﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace feb26
{
    class Program
    {
        static void Main(string[] args)
        {
             /// creating object for connection
            SqlConnection con = null;

            ///connecting to database
            ///parameter for connection
            ///data source = server name 
            ///initial catalog = database name 
            ///user id = username of sql server
            ///password = password of sql server 
            con = new SqlConnection("data source = LAPR256; integrated security = false; initial catalog = Bikes; user id = sa; password = prabhat@123");

            try
            {
                ///opening the server connection for data flow 
                con.Open();

                ///creating table
                //SqlCommand command = new SqlCommand("create table person(" +
                //       "id int," +
                //      "_name varchar(23)," +
                //       "dob varchar(11))", con);

                //command.ExecuteNonQuery(); //this command will execute above query 


                string s = "prabhat";
                ///inserting into table 
                SqlCommand comm = new SqlCommand("insert into person values(3,' "+ s + "','07-11-1997')", con);
                comm.ExecuteNonQuery();

                ///deleting from table 
                comm = new SqlCommand("delete from person where id = 2",con);
                comm.ExecuteNonQuery();
                
                ///updateing the table
                comm = new SqlCommand("update person set _name = 'arif' where id = 3", con);
                comm.ExecuteNonQuery();

                ///fetching data from table 
                SqlDataAdapter sql = new SqlDataAdapter("select * from person", con);



                ///table object for storing data that is being fetched from database 
                ///make fetched data readable
                DataTable dtb = new DataTable();

                /// storing data from the fetched query sql
                sql.Fill(dtb);

                ///iterating and prining the fetched quyery 
                foreach(DataRow row in dtb.Rows)
                {
                    Console.WriteLine(row["id"] + " " +row["_name"]);
                }

                Console.WriteLine("connection Establish");


                ///creating procedure
                comm = new SqlCommand("create procedure prabhat @para varchar(25)" +
                    "as" +
                    "insert into production.brands(brand_name) value(@para);", con);


                ///Running pre built procedure
                ///procedure to insert new value
                comm = new SqlCommand("prabhat", con);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add(new SqlParameter("@para","anshu"));
                comm.ExecuteNonQuery();
                sql = new SqlDataAdapter("select * from production.brands", con);

                sql.Fill(dtb);

                foreach(DataRow row in dtb.Rows)
                {
                    Console.WriteLine(row["brand_id"] + " " + row["brand_name"]);
                }


            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                ///closing connecting
                con.Close();
            }
            Console.ReadKey();
        }
    }
}
