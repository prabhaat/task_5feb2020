﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Joins_Linq
{
    /// <summary>
    /// Class having two property name and age
    /// </summary>
    class ShardaStudents
    {
        public string Name { get; set; }
        public int Id { get; set; }

    }

    /// <summary>
    /// class with two properties name and age
    /// </summary>
    class AmityStudent
    {
        public string Name { get; set; }
        public int Id { get; set; }

    }
    class LinqJoin
    {

        /// <summary>
        /// function for inner join 
        /// </summary>
        public static void InnerJoin()
        {

            /// list Containing object list of class shardastudents
            List<ShardaStudents> studentList_1 = new List<ShardaStudents>()
            {
                new ShardaStudents() { Name = "Prabhat",Id = 1 },
                new ShardaStudents() { Name = "Ayush",Id = 2 },
                new ShardaStudents() { Name = "Akash",Id = 3 },
                new ShardaStudents() { Name = "Ashish",Id = 4 },
                new ShardaStudents() { Name = "Arif",Id = 5 },
                new ShardaStudents() { Name = "Vishal",Id = 6 },
                new ShardaStudents() { Name = "Vivek",Id = 7 },
                new ShardaStudents() { Name = "Yash",Id = 8 }
            };

            /// list Containing object list of class Ammitystudent
            List<AmityStudent> studentList_2 = new List<AmityStudent>()
            {
                new AmityStudent() { Name = "Maurya",Id = 1 },
                new AmityStudent() { Name = "Agrawal",Id = 12 },
                new AmityStudent() { Name = "kohar",Id = 3 },
                new AmityStudent() { Name = "Jain",Id = 40 },
                new AmityStudent() { Name = "ahmed",Id = 57 },
                new AmityStudent() { Name = "singh ",Id = 6 },
                new AmityStudent() { Name = "sharma",Id = 77 },
                new AmityStudent() { Name = "Aggrawal",Id = 8 }
            };




            //linq query for inner join of those two list
            var Joined = (from student in studentList_1
                          join people in studentList_2
                          on student.Id equals people.Id
                          select new
                          {
                              studentName = student.Name,
                              peopleName = people.Name
                          });

            //Console.WriteLine("First_name  Last_Name");


            //printing the joined List
            foreach (var student in Joined)
            {
                Console.WriteLine("\t" + student.studentName + " " + student.peopleName);
            }


        }

        /// <summary>
        /// Function for Left Join
        /// </summary>
        public static void LeftJoin()
        {
            List<ShardaStudents> studentList_1 = new List<ShardaStudents>()
            {
                new ShardaStudents() { Name = "Prabhat",Id = 1 },
                new ShardaStudents() { Name = "Ayush",Id = 2 },
                new ShardaStudents() { Name = "Akash",Id = 3 },
                new ShardaStudents() { Name = "Ashish",Id = 4 },
                new ShardaStudents() { Name = "Arif",Id = 5 },
                new ShardaStudents() { Name = "Vishal",Id = 6 },
                new ShardaStudents() { Name = "Vivek",Id = 7 },
                new ShardaStudents() { Name = "Yash",Id = 8 }
            };

            List<AmityStudent> studentList_2 = new List<AmityStudent>()
            {
                new AmityStudent() { Name = "Maurya",Id = 1 },
                new AmityStudent() { Name = "Agrawal",Id = 12 },
                new AmityStudent() { Name = "kohar",Id = 3 },
                new AmityStudent() { Name = "Jain",Id = 40 },
                new AmityStudent() { Name = "ahmed",Id = 57 },
                new AmityStudent() { Name = "singh ",Id = 6 },
                new AmityStudent() { Name = "sharma",Id = 77 },
                new AmityStudent() { Name = "Aggrawal",Id = 8 }
            };

            ///Linq Query for LEft Join
            var joinedList = from student in studentList_1
                             join person in studentList_2
                             on student.Id equals person.Id into studList
                             from people in studList
                             select new { people.Name, people.Id };

            ///printing the joined Query
            foreach (var obj in joinedList)
            {
                Console.WriteLine(obj.Id + obj.Name);
            }

        }

        /// <summary>
        /// Function in Group Join
        /// </summary>
        public static void GroupJoin()
        {
            List<ShardaStudents> studentList_1 = new List<ShardaStudents>()
            {
                new ShardaStudents() { Name = "Prabhat",Id = 1 },
                new ShardaStudents() { Name = "Ayush",Id = 2 },
                new ShardaStudents() { Name = "Akash",Id = 3 },
                new ShardaStudents() { Name = "Ashish",Id = 4 },
                new ShardaStudents() { Name = "Arif",Id = 5 },
                new ShardaStudents() { Name = "Vishal",Id = 6 },
                new ShardaStudents() { Name = "Vivek",Id = 7 },
                new ShardaStudents() { Name = "Yash",Id = 8 }
            };

            List<AmityStudent> studentList_2 = new List<AmityStudent>()
            {
                new AmityStudent() { Name = "Maurya",Id = 1 },
                new AmityStudent() { Name = "Agrawal",Id = 12 },
                new AmityStudent() { Name = "kohar",Id = 3 },
                new AmityStudent() { Name = "Jain",Id = 40 },
                new AmityStudent() { Name = "ahmed",Id = 57 },
                new AmityStudent() { Name = "singh ",Id = 6 },
                new AmityStudent() { Name = "sharma",Id = 77 },
                new AmityStudent() { Name = "Aggrawal",Id = 8 }
            };

            ///Linq Query for Group Inner Join 
            var QueryList = from students in studentList_1
                            join persons in studentList_2 on students.Id equals persons.Id
                            into joinedList
                            select new
                            {
                                firstName = students.Name,
                                lastName = from list in joinedList
                                           orderby list.Name
                                           select list
                            };

           ///Quering through the List
            foreach (var Querys in QueryList)
            {
                Console.WriteLine(Querys.firstName);

                foreach (var query in Querys.lastName)
                {
                    Console.WriteLine("{0,-10} {1}", query.Name, query.Id);
                }

            }

        }
    }
}
