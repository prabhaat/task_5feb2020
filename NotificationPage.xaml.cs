﻿using NotificationTask.Model;
using NotificationTask.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NotificationTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificationPage : ContentPage
    {
        //List<DataModel> dataModels = new List<DataModel>
        //{
        //    new DataModel(){Name = "Dana Jhonson",Changed = "Not Started",ColorText = Color.Red},
        //    new DataModel(){Name = "Dana Jhonson",Changed = "Not Started",ColorText = Color.Red},
        //    new DataModel(){Name = "Dana Jhonson",Changed = "Not Started",ColorText = Color.Red},
        //    new DataModel(){Name = "Dana Jhonson",Changed = "Not Started",ColorText = Color.Red},
        //};

        FunctionViewModel function = new FunctionViewModel();
        public NotificationPage()
        {
            InitializeComponent();
            BindingContext = function;


            listView.ItemsSource = function.GetList();
            
        }
    }
}