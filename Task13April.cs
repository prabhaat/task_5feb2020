﻿using LoginApp_31_march_2020_.Anime;
using LoginApp_31_march_2020_.Movies;
using LoginApp_31_march_2020_.Tv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginApp_31_march_2020_
{
    [XamlCompilation(XamlCompilationOptions.Compile)]


    public class Genre
    {
        public String MoviesGenre { get; set; }
    }

   
    public partial class NewUI : ContentPage
    {

        List<Genre> genres = new List<Genre>
             {
                 new Genre{MoviesGenre = "Drama"},
                 new Genre{MoviesGenre = "Action"},
                 new Genre{MoviesGenre = "Adventure"},
                 new Genre{MoviesGenre = "Thriller"},
                 new Genre{MoviesGenre = "Romance"}
             };

        MoviesPage moviesPage = new MoviesPage();
        TvData Tv = new TvData();
        AnimeList anime = new AnimeList();
        public NewUI()
        {


            InitializeComponent();
            listView.ItemsSource = genres;
            listView.SelectedItem = new EventHandler(Hello);


           
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            displayData.Content = moviesPage.OnlyContentView();
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            displayData.Content = Tv.Content;
        }
        public void Hello(object sender,EventArgs e)
        {
            DisplayAlert("Welcome", "This is Selected Example", "Ok");
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            displayData.Content = anime.Content;
        }
    }
}