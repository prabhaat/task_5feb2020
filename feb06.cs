﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//abstract class that contains structure of the child classes
abstract class Party
{

 #region properties 
    
    public int catering = 100;  
    public int decoration = 500;
    public string date;
    public int days = 1;
    public const int minimum = 100;
    public int expenditure;
    public int charges;
  
 #endregion


 #region Methods
    public virtual void Calculate()
    {

    }

#endregion

}
//class for MArriage party property
class Marriage : Party
{
    
    Package package = new Package();
    public int guestIncoming;     //number of guest coming
    int _extraGuest;              //for calculating more than base prize 
    int _minExpenditure = 80000;  //minimum cost of marriage, for less than 100 guest 
    int _addOnExpenditure;       //per plate cost of extra guest
    string _choice;


    /// <summary>
    /// Method to calculate expenditure, the cost Doing Marriage 
    /// </summary>
    public override void Calculate()
    {

        Console.WriteLine("\t\tDo you want to Book with Package? ::\n");
        _choice = Console.ReadLine();
        //choice for packages
        switch (_choice)
        {
            case "yes":
                choices:
                Console.WriteLine("\t\tpress the following option:: \n\t\tselect Package \n\t\t1.Silver\n\t\t2.Gold\n\t\t3.Platinum");
                _choice = Console.ReadLine();
                switch (_choice)
                {
                    case "1": expenditure = package.Silver(); break;   
                    case "2": expenditure = package.Gold(); break;
                    case "3": expenditure = package.Platinum(); break;
                    default:
                        Console.WriteLine("Please Follow the instructions");
                        goto choices;
                }
                break;

            default:
                Console.WriteLine("you have not selected any pakages ");
                break;
        }


        ///calculating guest on the basis of incoming and base class 
        if (guestIncoming > 100)
            _extraGuest = guestIncoming - minimum;

        else
        {
            _extraGuest = 0;
        }

        expenditure = days * _minExpenditure;
        _addOnExpenditure = _extraGuest * catering;
        expenditure = expenditure + _addOnExpenditure;
        Console.WriteLine("\t\t** your expendure is {0} \nyour party is confirmed on {1} **", expenditure, date);

    }

}


/// <summary>
/// corporate class 
/// </summary>
class Corperate : Party
{
    // int cake = 250;
    Package package = new Package();
    public int guestIncoming;  //number of guest coming to party
    int _extraGuest;           //guest more than the guest incoming 
    int _minExpenditure = 18000;   //min expenditure for 18000
    int _addOnExpenditure;  //add on expenditure
    int _expenditure;


    string _choice;

    /// <summary>
    /// calculate the expences on corporate party
    /// </summary>
    public override void Calculate()
    {

        Console.WriteLine("\t\tDo you want to Book with Package? ::\n");
        _choice = Console.ReadLine();
        switch (_choice)
        {
            case "yes":
                choices:
                Console.WriteLine("\t\tpress the following option:: \n\t\tselect Package \n\t\t1.Silver\n\t\t2.Gold\n\t\t3.Platinum");
                _choice = Console.ReadLine();

                switch (_choice)
                {
                    case "1": expenditure = package.Silver(); break;
                    case "2": expenditure = package.Gold(); break;
                    case "3": expenditure = package.Platinum(); break;
                    default:
                        Console.WriteLine("Please Follow the instructions");
                        goto choices;
                }
                break;

            default:
                Console.WriteLine("you have not selected any pakages ");
                break;
        }

        if (guestIncoming > 100)
            _extraGuest = guestIncoming - minimum;

        else
        {
            _extraGuest = 0;
        }

        expenditure = days * _minExpenditure;
        _addOnExpenditure = _extraGuest * catering;
        expenditure = expenditure + _addOnExpenditure;
        Console.WriteLine("\t\t** your expendure is {0} \nyour party is confirmed on {1} **", expenditure, date);

    }

}


class Birthday : Party
{
    // int cake = 250;
    Package package = new Package();
    public int guestIncoming;
    int _extraGuest;
    int _minExpenditure = 8000;

    int _addOnExpenditure;
    int _expenditure;

    string _choice;
    public override void Calculate()
    {

        Console.WriteLine("\t\tDo you want to Book with Package? ::\n");
        _choice = Console.ReadLine();
        switch (_choice)
        {
            case "yes":
                choices:
                Console.WriteLine("\t\tpress the following option:: \n\t\tselect Package \n\t\t1.Silver\n\t\t2.Gold\n\t\t3.Platinum");
                _choice = Console.ReadLine();

                switch (_choice)
                {
                    case "1": expenditure = package.Silver(); break;
                    case "2": expenditure = package.Gold(); break;
                    case "3": expenditure = package.Platinum(); break;
                    default:
                        Console.WriteLine("Please Follow the instructions");
                        goto choices;
                }
                break;

            default:
                Console.WriteLine("you have not selected any pakages ");
                break;
        }





        if (guestIncoming > 100)
            _extraGuest = guestIncoming - minimum;

        else
        {
            _extraGuest = 0;
        }

        expenditure = days * _minExpenditure;
        _addOnExpenditure = _extraGuest * catering;
        expenditure = expenditure + _addOnExpenditure;
        Console.WriteLine("\t\t** your expendure is {0} \nyour party is confirmed on {1} **", expenditure, date);

    }

}


/// <summary>
/// Class package containing methods 
/// for calculating expenses on silver, platinum , gold
/// </summary>
class Package : Party
{
    int _expenditure;


    /// <summary>
    /// for calculating expenses of silver package
    /// </summary>
    /// <returns> Returns expenditure for silver package  </returns>
    public int Silver()
    {
        _expenditure = minimum * 800;
        return _expenditure;
    }
    /// <summary>
    /// for calculating expenses of Platinum package
    /// </summary>
    /// <returns> Returns expenditure for Platinum package  </returns>
    public int Platinum()
    {
        _expenditure = minimum * 1200;
        return _expenditure;
    }
    /// <summary>
    /// for calculating expenses of Gold package
    /// </summary>
    /// <returns> Returns expenditure for Gold package  </returns>
    public int Gold()
    {
        _expenditure = minimum * 1000;
        return _expenditure;
    }
}


namespace feb06
{
    class Program
    {
        static void Main(string[] args)
        {
            int _guestIncoming;
            int _days;
            int _date;
            string _choice;
            //creating object of marriage class
            Marriage marriage = new Marriage();
            //creating object of birthday class
            Birthday birthday = new Birthday();
            //creating object of corporate class
            Corperate corporate = new Corperate();
            bool choiceMade = false;      
            PrintTitle();

            int t = 1;


            /// user interface
            while (t == 1)
            {


                Console.WriteLine("\t\tPlease Enter the following choices To continue: \n\t\tMarriage \n\t\tBirthday");
                //Console.WriteLine("\t\t");
                _choice = Console.ReadLine();
                _choice = _choice.ToUpper();
                switch (_choice)
                {

                    case "MARRIAGE":
                        choiceMade = true;
                        Console.WriteLine("\t\tplease enter number of guest\n");
                        marriage.guestIncoming = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\t\tplease enter number of days\n");
                        marriage.days = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\t\tplease enter the date in fornat (DD/MM/YYYY)\n");
                        marriage.date = Console.ReadLine();
                        checkdate(marriage.date);
                        marriage.Calculate();
                        break;
                    case "BIRTHDAY":
                        choiceMade = true;
                        Console.WriteLine("\t\tplease enter number of guest\n");
                        birthday.guestIncoming = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\t\tplease enter the date\n");
                        birthday.date = Console.ReadLine();
                        birthday.Calculate();
                        break;

                    case "CORPORATE":
                        choiceMade = true;
                        Console.WriteLine("\t\tplease enter number of guest\n");
                        corporate.guestIncoming = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\t\tplease enter the date\n");
                        corporate.date = Console.ReadLine();
                        corporate.Calculate();
                        break;


                    default:
                        Console.WriteLine("\t\tNOTE:::please choose the given options :: .....\n");
                        Console.WriteLine("\t\tIf you wish to exits Press 1\n\t\tElse press Any Key to Continue : ");

                        _choice = Console.ReadLine();
                        _choice = _choice.ToUpper();

                        switch (_choice)
                        {
                            case "1":
                                choiceMade = false;
                                break;

                            default:
                                choiceMade = true;
                                break;

                        }

                        if (!choiceMade)
                        {
                            Console.WriteLine("\t\t Thank you for using our site");
                            break;
                        }

                        continue;


                }
                if (!choiceMade)
                {
                    break;
                }


                Console.WriteLine("do you wish to continue? \npress 1 for yes \npress 2 for no");
                t = Convert.ToInt32(Console.ReadLine());

            }


        }

        /// <summary>
        /// print the title of the main console.
        /// </summary>
        public static void PrintTitle()
        {
            Console.WriteLine("\t\t****************************************************************************************\n");
            Console.WriteLine("\t\t*\t\tWelcome to the the Party BOOKING WORLD\t\t\t\t\t*\n");
            Console.WriteLine("\t\t*                                                                                       *\n");
            Console.WriteLine("\t\t*\t\tWe specialize in making your party Memorable.\t\t\t\t*\n");
            Console.WriteLine("\t\t****************************************************************************************\n");


        }

        public static void checkdate(string a)
        {
           // int length = a.Length();
            //int day = Convert.ToInt32(a[0] + a[1]);
            
            
            


        }

    }


}