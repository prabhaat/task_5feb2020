﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginApp_31_march_2020_.BindLayout
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public class Trainees
    {
        public string Name { get; set; }
    }

    public class WatchListItem
    {
        public string Title { get; set; }
        public string Src { get; set; }
    }
    public partial class BindLayoutPRactice : ContentPage
    {
        List<WatchListItem> watchListItems = new List<WatchListItem>
        {
            new WatchListItem{Title = "Mission Impossible",Src = "pngwave.png"},
            new WatchListItem{Title = "Naruto",Src = "Naruto.jpg"},
            new WatchListItem{Title = "Full Metal ALchemist",Src = "FullMetal.jpg"},
            new WatchListItem{Title = "F.R.I.E.N.D.S",Src = "Friends.jpg"},
            new WatchListItem{Title = "Good Will Hunting", Src="GoodwillHunting.png"},
            new WatchListItem{Title = "Stranger Things",Src = "Stranger_Things.png"}
        };


        List<Trainees> trainees = new List<Trainees>
        {
            new Trainees{Name = "Prabhat"},
            new Trainees{Name = "Arif"},
            new Trainees{Name = "Ayush"},
            new Trainees{Name = "AAshish"},
            new Trainees{Name = "Akash"}
        };

        public BindLayoutPRactice()
        {
            InitializeComponent();

            

            BindableLayout.SetItemsSource(bind, watchListItems);
           

           

        }
    }
}