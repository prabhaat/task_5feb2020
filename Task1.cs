﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



/// <summary>
/// abstract class containing all the info the other 
/// classes or we can say a blueprint
/// </summary>
abstract class Vehicle
{
    #region property
    public string vehicleName;
    public int vehicleWheel;
    public int vehicleSpeed;
    public int tollAmount;
    public string vehicleColor;
    public const int MaxSpeed = 300;
    #endregion

#region methods
    /// <summary>
    /// function to be overridden in child class
    /// </summary>
    public virtual void Start()
    {

    }
    /// <summary>
    /// function to be overriden in child class
    /// </summary>
    /// <param name="Speed"> integer type, value by which we need to increase the speed  </param>
    public virtual void SpeedUp(int Speed)
    {

    }
    /// <summary>
    /// method to be overriden in child class
    /// </summary>
    public virtual void stop()
    {

    }
    public virtual void CalculateTollAmount()
    {

    }
    public Vehicle()
    { }
#endregion
}
class Car : Vehicle
{
    /// <summary>
    /// constructor of class Car. 
    /// parameterized so that the value of each variable is set after initializing of object 
    /// </summary>
    /// <param name="(string)VehicleName"> name of the vehicle </param>
    /// <param name="(string)Vehiclecolor"> color of the vehicle </param>
    /// <param name="(int)VehicleWheel"> number of wheel of vehicle</param>
   public Car(string VehicleName,string Vehiclecolor,int VehicleWheel)
    {
        vehicleName = VehicleName;
        vehicleColor = Vehiclecolor;
        vehicleWheel = VehicleWheel;
    }

    /// <summary>
    /// start the vehicle and
    /// print the information of vehicle
    /// </summary>
    public override void Start()
    {
        Console.WriteLine("your {0} of {1} color having {2} have been started ", vehicleName, vehicleColor, vehicleWheel);

    }
    /// <summary>
    /// stop the vehicle and print the information
    /// </summary>
    public override void stop()
    {
        Console.WriteLine("your {0} of {1} color having {2} has been stopped ", vehicleName, vehicleColor, vehicleWheel);
    }
    /// <summary>
    /// increase the speed of your car
    /// and prints the information
    /// </summary>
    /// <param name="Speed"> value by which you need to increase the speed</param>
    public override void SpeedUp(int Speed)
    {
        vehicleSpeed = vehicleSpeed + Speed;
        Console.WriteLine("your {0} of {1} color having {2} has " +
            "been speedup to {3} ", vehicleName, vehicleColor, vehicleWheel,vehicleSpeed);
    }

    /// <summary>
    /// To Calculate the toll amount 
    /// </summary>
    public override void CalculateTollAmount()
    {
        tollAmount = vehicleWheel * 15;
        Console.WriteLine("The Value of toll Amount of Car is {0}", tollAmount);
    }
}


class Bike : Vehicle
{


    /// <summary>
    /// constructor of class Bike. 
    /// parameterized so that the value of each variable is set after initializing of object 
    /// </summary>
    /// <param name="(string)VehicleName"> name of the vehicle </param>
    /// <param name="(string)Vehiclecolor"> color of the vehicle </param>
    /// <param name="(int)VehicleWheel"> number of wheel of vehicle</param>

   public Bike(string VehicleName, string Vehiclecolor, int VehicleWheel)
    {
        vehicleName = VehicleName;
        vehicleColor = Vehiclecolor;
        vehicleWheel = VehicleWheel;
    }

    /// <summary>
    /// start the vehicle and
    /// print the information of vehicle
    /// </summary>

    public override void Start()
    {
        Console.WriteLine("your {0} of {1} color having {2} have been started ", vehicleName, vehicleColor, vehicleWheel);

    }

    /// <summary>
    /// stop the vehicle and print the information
    /// </summary>
    public override void stop()
    {
        Console.WriteLine("your {0} of {1} color having {2} has been stopped ", vehicleName, vehicleColor, vehicleWheel);
    }


    /// <summary>
    /// increase the speed of your car
    /// and prints the information
    /// </summary>
    /// <param name="Speed"> value by which you need to increase the speed</param>
    public override void SpeedUp(int Speed)
    {
        vehicleSpeed = vehicleSpeed + Speed;
        Console.WriteLine("your {0} of {1} color having {2} has " +
            "been speedup to {3} ", vehicleName, vehicleColor, vehicleWheel, vehicleSpeed);
    }

    /// <summary>
    /// To Calculate the toll amount 
    /// </summary>
    public override void CalculateTollAmount()
    {
        tollAmount = vehicleWheel * 15;
        Console.WriteLine("The Value of toll Amount of Bike is {0}", tollAmount);
    }

}



namespace OOPs
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("civic","white",4);
            car.Start();
            car.SpeedUp(30);
            car.stop();
            car.CalculateTollAmount();
            Console.WriteLine("\n");
            Bike bike = new Bike("duke", "white", 2);
            bike.CalculateTollAmount();
            Console.WriteLine("\n");
            Console.ReadKey();

        }
    }
}
