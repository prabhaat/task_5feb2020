﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FileHandlingApp
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "temp.txt");

        string jsonFile = "firstTest.json";



        Int64 count = -1;
        public MainPage()
        {
            InitializeComponent();
            btn.Text = "Save";
            btn.IsEnabled = false;
            displayBtn.IsEnabled = false;
            inputField.TextChanged += (sender, e) =>
            {
                var obj = (Entry)sender;
                if (!string.IsNullOrEmpty(obj.Text))
                    btn.IsEnabled = true;
                else
                    btn.IsEnabled = false;



            };

            btn.Clicked += (sender, e) =>
            {
                if (File.Exists(fileName))
                { 
                    File.WriteAllText(fileName, inputField.Text);
                    displayBtn.IsEnabled = true;
                }

                else
                    display.Text = "No Such File Exist";


               
            };

            displayBtn.Clicked += (sender, e) =>
            {
                display.Text = File.ReadAllText(fileName);
                displayBtn.IsEnabled = false;
            };
           
        }
    }

    
}
