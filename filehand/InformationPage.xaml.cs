﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FileHandlingApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InformationPage : ContentPage
    {
        MainPage page = new MainPage();
        EmployeeContent employee = new EmployeeContent();
        public InformationPage()
        {
            InitializeComponent();
            textFile.Clicked += (sender, e) =>
            {
                contentView.Content = page.Content;
            };
            jsonFile.Clicked += (sender, e) =>
            {
                contentView.Content = employee.Content;
            };
        }
    }
}