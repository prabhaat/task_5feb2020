﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FileHandlingApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class EmployeeContent : ContentPage
    {
        DataList dataList = new DataList();

        List<TestingFile> jsonlist;
        public List<TestingFile> GetList()
        {
            string filename = "firstTest.json";
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(EmployeeContent)).Assembly;
            Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.{filename}");

            using (var rdr = new StreamReader(stream))
            {
                var data = rdr.ReadToEnd();
                var list = JsonConvert.DeserializeObject<DataList>(data);

                jsonlist = list.Emplyoee;

            }

            return jsonlist;
        }
        public EmployeeContent()
        {
            InitializeComponent();

            listView.ItemsSource = GetList();
           
        }
    }


    public class TestingFile
    {
        public string Name { get; set; }
        public string Department { get; set; }
    }

    public class DataList
    {
        public List<TestingFile> Emplyoee { get; set; }
    }
}