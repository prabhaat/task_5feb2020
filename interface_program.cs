﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


interface IPaint
{
    void Paint();

}
interface IChangeSeatCover
{
    void ChangeCover();
}
public class Vehicle : IPaint, IChangeSeatCover
{
    string vehicleName;     //name of the vehicle

    string color;           //color of vehicle

    //setting property of variable color
    //since color is private we cannot access it directly on object
    //so we use property method provided in c# to set its value
    //property setColor to set the value of color of the vehicle
    public string setColor
    {
        get { return color; }
        set
        {
            color = value;
        }
    }

    enum wheel
    {
        Car = 4,
        bike = 2,
        auto = 3,
        bus = 6
    };


    wheel VehicleWheel = wheel.Car;

    //using property set wheel to set the value of wheel since it is private


    int speed = 0;                     //initial speed 0

    bool isStop = true;                //currently car is stopped

    public const int maxSpeed = 300;   //limit of max speed



    //constructor will set the name of the vehicle
    //since constructor can be invoked once per object 
    //pros : the name will be attched to the object
    public Vehicle(string vehicleName)
    {
        this.vehicleName = vehicleName;
    }

    public void ShouldChange()
    {
        bool IsChange = false;
        string changeChoice;
        String choice;
        
        Console.WriteLine("Do you want to change the Color or Seat?? \nPess Y(yes) if you want to \nand\nN(no) if you want to exit");
        choice = Console.ReadLine();
        choice = choice.ToUpper();
        
        switch (choice)
        {
            case "Y": 
                   
                  Console.WriteLine(" please Select the following you want to change:: \n1.Cover \n2.Color");
                  changeChoice = Console.ReadLine();
                  changeChoice = changeChoice.ToUpper();
                  switch(changeChoice)
                {
                    case "COVER":

                                   ChangeCover();
                                    break;
                    case "COLOR":  Paint();
                                         break;

                    default: Console.WriteLine("oops! People do miss the details. I sugguest read the intruction carefully to avoid inconvience");

                        break;
                }

                break;
            case "N": Console.WriteLine("thankyou for using our services");
                break;
            default: Console.WriteLine("please follow the instruction carefully ");
                break;
        }

    }

    //method to start the car 
    public void Start()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been started \nspeedup" +
                           "to move forward", color, vehicleName, VehicleWheel);

        speed = 0;       //since car is started so initial value 0

        isStop = false;   //setting the value of if car is stopped or not as false    
    }
    public void Stop()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been stopped", color, vehicleName, VehicleWheel);

        speed = 0;            //stopped speed 0

        isStop = true;       //car is stopped so the variable is true
    }



    //Method will speed up the car by adding provided speed to the current speed of vehicle
    public void SpeedUp(int speed)          //integer type parameter, argument is speed by which 
    {                                       //we are increasing current speed
        //if car is stopped we can't increase speed
        //can't have negative value of speed 
        if (!isStop && speed >= 0)
        {
            //making sure that speed is less then maxspeed
            if (this.speed <= maxSpeed)
            {
                Console.WriteLine("speed of your {0} having {1} wheel has been incresed by" +
                                    " {2}", vehicleName, speed);

                this.speed = this.speed + speed;     //increasing speed

                //if speed is greater than maxSpeed 
                if (this.speed > maxSpeed)
                    this.speed = maxSpeed;

                Console.WriteLine("current speed of your vehicle is {0}", this.speed);
            }
        }
        if (speed < 0)
        {
            Console.WriteLine("invalid speed, please enter the positive integer value");
        }

    }

    public void ChangeCover()
    {
        string cover, colour;
        Console.WriteLine("Please select the type of cover ::: \n1.plastic\n2.Nylon\n3.Leather");
        cover = Console.ReadLine();
        Console.WriteLine("Please select the colour");
        colour = Console.ReadLine();
        Console.WriteLine($"the cover of car has been changed to {colour} color in {cover} cover");



    }





    public void Paint()
    {
        string color;
        Console.WriteLine("please the colour you want to paint your car with");
        color = Console.ReadLine();
        Console.WriteLine($"Your car has been painted {color} color");

        // throw new NotImplementedException();
    }
}

namespace Task
{

    class Program
    {
        static void Main(string[] args)
        {

            Vehicle car = new Vehicle("civic");      //making object of Vehicle
            car.setColor = "blue";                   //using property setcolor
            car.Start();                             //start method
            car.SpeedUp(20);                         //speed method
            car.SpeedUp(10);
            car.Stop();
            car.SpeedUp(50);
            Console.WriteLine("\n");
            Console.ReadKey();
        }

    }
}
