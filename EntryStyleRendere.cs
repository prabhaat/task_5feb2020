﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;
using Xamarin.Forms.PlatformConfiguration.WindowsSpecific;
using Xamarin.Essentials;
using Xamarin.Forms.StyleSheets;
using Windows.UI.Xaml.Media;
using Windows.UI;
using LoginApp_31_march_2020_.UWP;
using LoginApp_31_march_2020;

[assembly: ExportRenderer(typeof(EntryStyle), typeof(EntryStyleRenderer))]
namespace LoginApp_31_march_2020_.UWP
{
    public class EntryStyleRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control!=null)
            {
                Control.Background = new SolidColorBrush(Colors.White);
                Control.BackgroundFocusBrush = new SolidColorBrush(Colors.WhiteSmoke);
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(0);
            }

        }

    }
}
