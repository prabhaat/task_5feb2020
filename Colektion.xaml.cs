﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginApp_31_march_2020_.CollectionViewPractice
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public class WatchListItem
    {
        public string Title { get; set; }

        public string Src { get; set; }
    }
    public partial class Colektion : ContentPage
    {

        List<WatchListItem> watchListItems = new List<WatchListItem>
        {
            new WatchListItem{Title = "Mission Impossible",Src = "pngwave.png"},
            new WatchListItem{Title = "Naruto",Src = "Naruto.jpg"},
            new WatchListItem{Title = "Full Metal ALchemist",Src = "FullMetal.jpg"},
            new WatchListItem{Title = "F.R.I.E.N.D.S",Src = "Friends.jpg"},
            new WatchListItem{Title = "Good Will Hunting", Src="GoodwillHunting.png"},
            new WatchListItem{Title = "Stranger Things",Src = "Stranger_Things.png"}
        };
        public Colektion()
        {
            InitializeComponent();
            collectionview.ItemsSource = watchListItems;
            collectionview.ItemsLayout = new GridItemsLayout(2, ItemsLayoutOrientation.Vertical);

            
        }
    }
}