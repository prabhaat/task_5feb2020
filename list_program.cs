﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10feb2020_task3
{
    /// <summary>
    /// enum that contains months with equivalent month in integer
    /// </summary>
    public enum month
    {
        january = 1,
        febuary,
        march,
        april,
        may,
        june,
        july,
        august,
        september,
        october,
        november,
        december
    }
    class Program
    {
        static void Main(string[] args)
        {
            
            //dictionary which has key as enum months and value is number of days in months
            Dictionary<month, int> myDictionary = new Dictionary<month, int>();
            
            //Entering values in Dictionary
            myDictionary.Add(month.january, 31);
            myDictionary.Add(month.febuary, 28);
            myDictionary.Add(month.march, 31);
            myDictionary.Add(month.april, 30);
            myDictionary.Add(month.may, 31);
            myDictionary.Add(month.june, 30);
            myDictionary.Add(month.july, 31);
            myDictionary.Add(month.september, 30);
            myDictionary.Add(month.october, 31);
            myDictionary.Add(month.november, 30);
            myDictionary.Add(month.december, 31);

             //calling function to find number of days in  a month 
            findDay(myDictionary);

            Console.ReadKey();



        }


        /// <summary>
        /// function that print number of day according for entered month
        /// accepts dictionary as a parameter 
        /// </summary>
        /// <param name="myDictionary"> dictionary type as a parameter </param>
        public static void findDay(Dictionary<month, int> myDictionary)
        {
            Console.WriteLine("please enter the month");

            string Month = Console.ReadLine();
            
            //changes string value to the corresponding enum object/constant
            var value = (month)Enum.Parse(typeof(month), Month,true);

            //passing key in dictionary to find corresponding value
            var days = myDictionary[value];

            Console.WriteLine($" days in months {value} are {days} ");


        }




    }




}
