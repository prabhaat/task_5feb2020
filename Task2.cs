﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//class containing variables and methods 
public class Vehicle
{
    
#region private properties

    string vehicleName;     //name of the vehicle

    public string vehicleColor;           //color of vehicle

    public  int vehicleWheel;

    int speed = 0;                     //initial speed 0

    bool isStop = true;                //currently car is stopped
    #endregion

    
    //setting property of variable color
    //since color is private we cannot access it directly on object
    //so we use property method provided in c# to set its value
    //property setColor to set the value of color of the vehicle

    public string SetColor
    {
        get { return vehicleColor; }
        set
        {
            vehicleColor = value;
        }
    }

   public  Vehicle(string vehicleName)
    {
        this.vehicleName = vehicleName;
    }


    //using property set wheel to set the value of wheel since it is private
    public int SetWheel
    {
        get { return vehicleWheel; }
        set
        {
            vehicleWheel = value;
        }
    }

    readonly public int MaxSpeed = 300;    //limit of max speed

    
     


    #region public methods

    //constructor will set the name of the vehicle
    //since constructor can be invoked once per object 
    //pros : the name will be attched to the object


        

    //method to start the car 
         
    public void Start()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been started \nspeedup" +
                           "to move forward", vehicleColor, vehicleName, vehicleWheel);

        speed = 0;       //since car is started so initial value 0

        isStop = false;   //setting the value of if car is stopped or not as false    
    }
    public void Stop()
    {
        //information of car
        Console.WriteLine("your {0} colour {1} having {2} wheels has been stopped", vehicleColor, vehicleName, vehicleWheel);

        speed = 0;            //stopped speed 0

        isStop = true;       //car is stopped so the variable is true
    }



    //Method will speed up the car by adding provided speed to the current speed of vehicle
    public void SpeedUp(int speed)          //integer type parameter, argument is speed by which 
    {                                       //we are increasing current speed
        //if car is stopped we can't increase speed
        //can't have negative value of speed 
        if (!isStop && speed >= 0)
        {
            //making sure that speed is less then maxspeed
            if (this.speed <= MaxSpeed)
            {
                Console.WriteLine("speed of your {0} having {1} wheel has been incresed by" +
                                    " {2}", vehicleName, vehicleWheel, speed);

                this.speed = this.speed + speed;     //increasing speed

                //if speed is greater than maxSpeed 
                if (this.speed > MaxSpeed)
                    this.speed = MaxSpeed;

                Console.WriteLine("current speed of your vehicle is {0}", this.speed);
            }
        }
        if (speed < 0)
        {
            Console.WriteLine("invalid speed, please enter the positive integer value");
        }

    }
    

#endregion

}


/// <summary>
/// class having property of cars
/// class inherit all the public property and methods of Vehicle
/// </summary>
sealed class Car:Vehicle
{

    static string vehicleName = "Car" ;

    /// <summary>
    /// constructor of class car. 
    /// parameterized so that the value of each variable is set after initializing of object 
    /// </summary>
    /// <param name="(string)VehicleName"> name of the vehicle </param>
    /// <param name="(string)Vehiclecolor"> color of the vehicle </param>
    /// <param name="(int)VehicleWheel"> number of wheel of vehicle</param>
    public Car(string vehicleColor,int vehicleWheel,int maxSpeed):base(vehicleName)
    {
        this.vehicleColor = vehicleColor;
        this.vehicleWheel = vehicleWheel;
    }

    int tollAmount;

    /// <summary>
    /// function that calculate  the toll amount 
    /// </summary>
    public void calculateTollAmount()
    {
        tollAmount = 15 * vehicleWheel;
        Console.WriteLine("tollamount = {0}", tollAmount);

    }
    
    
    
}

/// <summary>
/// class having property of bike
/// class inherit all the public property and methods of Vehicle
/// </summary>
class Bike :Vehicle
{

    static string vehicleName = "bike";

    /// <summary>
    /// constructor of class Bike. 
    /// parameterized so that the value of each variable is set after initializing of object 
    /// </summary>
    /// <param name="(string)VehicleName"> name of the vehicle </param>
    /// <param name="(string)Vehiclecolor"> color of the vehicle </param>
    /// <param name="(int)VehicleWheel"> number of wheel of vehicle</param>
    public Bike(string vehicleColor, int vehicleWheel, int maxSpeed):base(vehicleName)
    {
        this.vehicleColor = vehicleColor;
        this.vehicleWheel = vehicleWheel;
    }


    int tollAmount;

    /// <summary>
    /// function to calculate the toll amount for bike
    /// </summary>
    public void calculateTollAmount()
    {
        tollAmount = 15 * vehicleWheel;
        Console.WriteLine("Your toll amount is {0}", tollAmount);
    }
    
     


}


namespace Task
{

    class Program
    {
        static void Main(string[] args)
        {
            Bike car = new Bike("red", 4, 300);

            car.Start();
            car.SpeedUp(10);
            car.Stop();
            car.calculateTollAmount();
            Console.WriteLine("\n");
            Console.ReadKey();
        }

    }

}
