﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginApp_31_march_2020.TestPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestPage : ContentView
    {
        public TestPage()
        {
            InitializeComponent();

            entry.Unfocused += async (obj, envt) =>
            {
                entry.Placeholder = label.Text;
                // await entry.ScaleTo(1, 100);
                bxView.IsVisible = false;
                label.IsVisible = false;
            };
        }

        static readonly BindableProperty TextProperty =
        BindableProperty.Create(nameof(Text), typeof(string), typeof(TestPage), defaultBindingMode: BindingMode.TwoWay,
        propertyChanged: (bindable, oldVal, newVal) =>
        {
            var text = bindable as TestPage;
            text.entry.Placeholder = (string)newVal;
            text.label.Text = (string)newVal;
        });

        static readonly BindableProperty UnderLineColorProperty =
        BindableProperty.Create(nameof(LineColor), typeof(Color), typeof(TestPage), defaultBindingMode: BindingMode.TwoWay,
        propertyChanged: (bindable, oldVal, newVal) =>
        {
            var text = bindable as TestPage;
            text.bxView.BackgroundColor = (Color)newVal;
        });

        static BindableProperty IsPasswordProperty =
        BindableProperty.Create(nameof(IsPassword), typeof(bool), typeof(TestPage),
        defaultValue: false, propertyChanged: (bindable, oldval, newval) =>
        {
             var ispass = bindable as TestPage;
             ispass.entry.IsPassword = (bool)newval;
        });

        static BindableProperty LeftImagePropert =
        BindableProperty.Create(nameof(LeftImage), typeof(string), typeof(TestPage),
        defaultBindingMode: BindingMode.TwoWay, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var img = bindable as TestPage;
            img.leftImage.Source = (string)newVal;
        });

        static BindableProperty RightImagePropert =
        BindableProperty.Create(nameof(RightImage), typeof(string), typeof(TestPage),
        defaultBindingMode: BindingMode.TwoWay, propertyChanged: (bindable, oldVal, newVal) =>
        {
            var img = bindable as TestPage;
            img.rightImage.Source = (string)newVal;
        });

        static BindableProperty CornerRadiusProperty =
        BindableProperty.Create(nameof(CurveRadius), typeof(float), typeof(TestPage),
        defaultBindingMode: BindingMode.TwoWay, propertyChanged: (bindable, oldVal, newVal) =>
          {
              var container = bindable as TestPage;
              container.frame.CornerRadius = (float)newVal;
          });



        public float CurveRadius
        {
            get
            {
                return (float)GetValue(CornerRadiusProperty);
            }
            set
            {
                SetValue(CornerRadiusProperty, value);
            }
        }

        public string RightImage
        {
            get
            {
                return (string)GetValue(RightImagePropert);
            }
            set
            {
                SetValue(RightImagePropert, value);
            }
        }

        public string LeftImage
        {
            get
            {
                return (string)GetValue(LeftImagePropert);
            }
            set
            {
                SetValue(LeftImagePropert, value);
            }
        }
        public bool IsPassword
        {
            get
            {
                return (bool)GetValue(IsPasswordProperty);
            }
            set
            {
                SetValue(IsPasswordProperty, value);
            }
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }

        public Color LineColor
        {
            get
            {
                return (Color)GetValue(UnderLineColorProperty);
            }
            set
            {
                SetValue(UnderLineColorProperty, value);
            }

        }




        private void Button_Clicked(object sender, EventArgs e)
        {
            entry.ScaleTo(2, 100);
        }

        private void entry_Focused(object sender, FocusEventArgs e)
        {
            entry.ScaleTo(1.1, 250);
            label.IsVisible = true;
            entry.Placeholder = "";
            bxView.IsVisible = true;

            // label.TranslateTo(-15, -2, 250);

            // entry.BackgroundColor = Color.GhostWhite;

        }
    }
}