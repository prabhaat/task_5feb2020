﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace WebApi.Controllers
{
    /// <summary>
    /// User defined action filter
    /// </summary>
    public class ActionFilter : ActionFilterAttribute
    {

        public string Id { get; set; }
        
        public ActionFilter(string actionName)
        {
            Id = actionName;
        }

        
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var action = actionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{Id} before = {action}");
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{Id} before = {action}");
        }



    }

    public class actionFilter2 : ActionFilterAttribute
    {
        public string Id { get; set; }

        public actionFilter2(string actionName)
        {
            Id = actionName;
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var action = actionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{Id} before = {action}");
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var action = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{Id} before = {action}");
        }
    }
    public class FilterController : ApiController
    {
        [ActionFilter("filter 1")]
        [Route("api/first/filter")]
        public string GetFirstActionFilter()
        {
            Trace.WriteLine("welcome to first filter");
            return "inside Get First Action Filter method";
        }
        [actionFilter2("filter 2")]
        [Route("api/second/filter")]
        public string GetSecondActionFilter()
        {
            Trace.WriteLine("welcome to Seccond filter");
            return "inside Get second Action Filter method";
        }
    }
}
