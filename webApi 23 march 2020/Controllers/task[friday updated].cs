﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAppCRUD.Models;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WebAppCRUD.Controllers
{
    public class PeopleController : ApiController
    {
             
        SqlConnection con = new SqlConnection("Data Source=LAPR256; Initial Catalog = bikes; User ID = sa; Password = prabhat@123");

        SqlCommand command;

        SqlDataReader dataReader;

        // GET: api/People
        public List<Person> Get()
        {
            List<Person> people = new List<Person>();

            string firstName, secondName;

            int id;
            
            try
            {
             
               con.Open();

               command = new SqlCommand("select * from studentDetails", con);

               dataReader = command.ExecuteReader();


            while (dataReader.Read())
            {
                firstName = dataReader["name"].ToString();

                secondName = dataReader["LastName"].ToString();

                id = Convert.ToInt32(dataReader["Id"].ToString());

                people.Add(new Person { FirstName = firstName, LastName = secondName, Id = id });
            }

            }
            catch(Exception e)
            {
                people.Add(new Person { FirstName = "NoOutput", LastName = "System Gave Exception", Id = 0 });
                
                Console.WriteLine("Code has given the Following Exception :\n", e);
            }
            finally
            {
                con.Close();
            }


            return people;
        }

        // GET: api/People/5
        public List<Person> Get(string id)
        { 

            List<Person> people = new List<Person>();

            try
            {

            con.Open();

            command = new SqlCommand("select * from studentDetails where id = '" +
                 id + "'", con);

            dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                people.Add(new Person
                {
                    FirstName = dataReader["name"].ToString(),
                    LastName = dataReader["lastName"].ToString(),
                    Id = Convert.ToInt32(dataReader["id"].ToString())
                }); ;
            }
            }
            catch(Exception e)
            {
                people.Add(new Person { FirstName = "No Output", LastName = "System threw Exception", Id = 0 });

                Console.WriteLine("System threw Following Exception: \n", e);
            }



            return people;
        }

        
        // POST: api/People
        public void Post(string firstName,string lastNmae,string id)
        {
            try
            {
                con.Open();

                command = new SqlCommand("insert into studentDetails(name,lastName,id) values('" +
                    firstName+"','" +
                    lastNmae+"','" +
                    id+"'", con);

                command.ExecuteNonQuery();

            }
            catch(Exception e)
            {
                Console.WriteLine("the system gave following Exception: \n", e);
            }
            finally
            {
                con.Close();
            }

        }

        // PUT: api/People/5
        public void Put(string id, [FromBody]Person student)
        {
            try
            {
                con.Open();

                command = new SqlCommand("update studentDetails set " +
                    "name = '" +
                    student.FirstName + "', lastname = '" +
                    student.LastName + "' where id  = '" +
                    student.Id + "'", con);

                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine("the system gave following Exception: \n", e);
            }
            finally
            {
                con.Close();
            }


        }

        // DELETE: api/People/5
        public void Delete(string id)
        {
            try
            {
                con.Open();

                command = new SqlCommand("delete from studentDetails where id = '" +
                    id+"'", con);

                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                Console.WriteLine("the system gave following Exception: \n", e);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
