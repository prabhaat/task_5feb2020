﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// person class having two member name and age
/// it has two property for setting the value of Name and Age 
/// </summary>
class Person
{
    string _name;
    int _age;

    public string Name
    {
        get { return _name; }
        set
        {
            _name = value;
        }
    }

    public int Age
    {
        get { return _age; }
        set
        {
            _age = value;
        }
    }
}


namespace _12feb2020_task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            ///creating object for person class and setting value
            Person person1 = new Person();
            person1.Name = "aashish";
            person1.Age = 61;

            Person person2 = new Person();
            person2.Name = "ayush";
            person2.Age = 62;

            Person person3 = new Person();
            person3.Name = "prabhat";
            person3.Age = 21;

            Person person4 = new Person();
            person4.Name = "arif";
            person4.Age = 45;

            Person person5 = new Person();
            person5.Name = "jyoti";
            person5.Age = 20;


            //adding the object in list
            List<Person> list = new List<Person>();
            list.Add(person1);
            list.Add(person2);
            list.Add(person3);
            list.Add(person4);
            list.Add(person5);

            //using foreach for enumerating each object 
            foreach(var person in list )
            {
                if(person.Age > 60)        //condition, so that only objects that has greater than 60
                {
                    Console.WriteLine($"Your name is  {person.Name}");
                }
            }


            //using lambda expression
            List<int> everynum = new List<int>();

            everynum.Add(12);
            everynum.Add(13);
            everynum.Add(14);
            everynum.Add(15);
            everynum.Add(16);
            everynum.Add(17);

            List<int> evennum = new List<int>();
            


            var newlist = list.Where(a => a.Age>60).Select(a=>a.Name);
            
            Console.WriteLine("Lambda expression");

            foreach(var name in newlist)
            {
                Console.WriteLine($"{name}");
            }

            Console.ReadKey();
        }
    }
}
