﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.Text.RegularExpressions;

namespace LoginScreen
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText editText;
        EditText pass;
        Button btn;
        ImageView image;
        ImageView image2;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
        }

        protected override void OnResume()
        {
            base.OnResume();
            editText = FindViewById<EditText>(Resource.Id.userName);
            editText.TextChanged += (obj, e) =>
            {
                var data = obj as EditText;
                OnTextChange(data.Text);
            };

            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
            alert.SetTitle("Welcome");
            alert.SetMessage("Hello");
            alert.SetNeutralButton("ok", (obj, e) =>
            {
                alert.Dispose();

            });

            Dialog dialog = alert.Create();

            btn = FindViewById<Button>(Resource.Id.btn);
            editText = FindViewById<EditText>(Resource.Id.userName);
            pass = FindViewById<EditText>(Resource.Id.password);
            btn.Click += (obj, e) =>
            {
                if(editText.Text == "abc@ab.ab" && pass.Text == "12345")
                   dialog.Show();
                

            };

        }

        void OnTextChange(string Name)
        {

            Regex rgx = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");

            if (string.IsNullOrEmpty(Name))
            {
                image = FindViewById<ImageView>(Resource.Id.eror);
                image.Visibility = Android.Views.ViewStates.Visible;
                image2 = FindViewById<ImageView>(Resource.Id.ok);
                image2.Visibility = Android.Views.ViewStates.Gone;
            }


            if (rgx.IsMatch(Name))
            {
                image = FindViewById<ImageView>(Resource.Id.eror);
                image.Visibility = Android.Views.ViewStates.Gone;
                image2 = FindViewById<ImageView>(Resource.Id.ok);
                image2.Visibility = Android.Views.ViewStates.Visible;
            }
            else
            {
                image = FindViewById<ImageView>(Resource.Id.eror);
                image.Visibility = Android.Views.ViewStates.Visible;
                image2 = FindViewById<ImageView>(Resource.Id.ok);
                image2.Visibility = Android.Views.ViewStates.Gone;
            }


           

        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}