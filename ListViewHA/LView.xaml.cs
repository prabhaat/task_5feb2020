﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections;
using System.ComponentModel;

namespace LoginApp_31_march_2020_.ListViewHA
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public class Genre : INotifyPropertyChanged
    {
        string _genre;
       
        public event PropertyChangedEventHandler PropertyChanged;
        public String MoviesGenre
        {
            get
            {
                return _genre;
            }
            set
            {
                _genre = value;
                OnPropertyChanged("MoviesGenre");
            }
        }

        Color _color;
        public Color bgColor 
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                OnPropertyChanged("bgColor");
            }
        }


        private void OnPropertyChanged(string status)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(status));
            }
        }




        //public Genre()
        //{
        //    bgColor = Color.Red;
        //}


    }
  

    public partial class LView : ContentPage
    {
        Genre pepe;

        ObservableCollection<Genre> genres = new ObservableCollection<Genre>
             {
                 new Genre{MoviesGenre = "Drama"},
                 new Genre{MoviesGenre = "Action"},
                 new Genre{MoviesGenre = "Adventure"},
                 new Genre{MoviesGenre = "Thriller"},
                 new Genre{MoviesGenre = "Romance"}
              };

        List<String> ls = new List<string>
        {
            "BAm",
            "Bam",
            "BAsadm",
            "BAasdm",
            "BAmsada"
        };

        //ViewCell lastCell;
        public LView()
        {

            InitializeComponent();

            listView.ItemsSource = genres;

            listView.SelectionMode = ListViewSelectionMode.Single;

            listView.ItemSelected += SelectList;


            pepe = genres[0];

        }

       
        public void SelectList(object sender, SelectedItemChangedEventArgs e)
        {
            var se = (ListView)sender;
            pepe.bgColor = Color.Transparent;
            int count = 0;
            pepe.bgColor = Color.Transparent;
            var data = e.SelectedItem as Genre;
            
            data.bgColor = Color.Red;
            pepe = data;


            //if (e.SelectedItem is Genre gen)
            //{

            //    foreach(var yo in genres)
            //    {
            //        if(yo.MoviesGenre == gen.MoviesGenre)
            //        {
            //            break;
            //        }
            //        count++;
            //    }

            //    genres[count].MoviesGenre = "Hello";

            //}



        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {

            var remEmp = (sender as MenuItem).CommandParameter as Genre;

            genres.Remove(remEmp);
        }

        //private void ViewCell_Tapped(object sender, EventArgs e)
        //{
        //    if (lastCell != null)
        //        lastCell.View.BackgroundColor = Color.Transparent;
        //    var viewCell = (ViewCell)sender;
        //    if (viewCell.View != null)
        //    {
        //        //viewCell.View.BackgroundColor = Color.Red;
        //        //lastCell = viewCell;

        //    }

        //}
    }
}
