﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LotyAnimationTask.CustomViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomLoader : ContentView
    {
        public CustomLoader()
        {
            InitializeComponent();
            InfinteRotation();
        }




        public static BindableProperty BarColorProperty =
     BindableProperty.Create(nameof(BarColor), typeof(Color), typeof(CustomLoader), defaultBindingMode: BindingMode.TwoWay,
         propertyChanged: (bindable, oldval, newVal) =>
         {
             var boxView = bindable as CustomLoader;
             boxView.bx.BackgroundColor = (Color)newVal;

         });
        public static BindableProperty LabelText =
        BindableProperty.Create(nameof(Text), typeof(String), typeof(CustomLoader), defaultBindingMode: BindingMode.TwoWay,
           propertyChanged: (bindable, oldval, newVal) =>
           {
               var boxView = bindable as CustomLoader;
               boxView.lbl.Text = (string)newVal;

           });
        public static BindableProperty LoaderColorProperty =
       BindableProperty.Create(nameof(LoaderColor), typeof(Color), typeof(CustomLoader), defaultBindingMode: BindingMode.TwoWay,
          propertyChanged: (bindable, oldval, newVal) =>
          {
              var frame = bindable as CustomLoader;
              frame.frameBottom.BackgroundColor = (Color)newVal;
              frame.frameTop.BackgroundColor = (Color)newVal;

          });

        public Color LoaderColor
        {
            get
            {
                return (Color)GetValue(LoaderColorProperty);
            }
            set
            {
                SetValue(LoaderColorProperty, value);
            }
        }

        public string Text
        {
            get
            {
                return (string)GetValue(LabelText);
            }
            set
            {
                SetValue(LabelText, value);
            }
        }

        public Color BarColor
        {
            get
            {
                return (Color)GetValue(BarColorProperty);
            }
            set
            {
                SetValue(BarColorProperty, value);
            }
        }


        async Task InfinteRotation()
        {
            while (true)
            {
                for (int i = 0; i < 7; i++)
                {
                    if (frameBottom.Rotation >= 360f)
                        frameBottom.Rotation = 0;
                    await frameBottom.RotateTo(i * (360 / 6), 100, Easing.Linear);
                }
            }
        }


    }
}